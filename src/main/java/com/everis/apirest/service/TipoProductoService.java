package com.everis.apirest.service;

import com.everis.apirest.model.entity.TipoProducto;

public interface TipoProductoService {
	
	public TipoProducto obtenerTipoProductoPorCodigo(String codigo) throws Exception;

}
