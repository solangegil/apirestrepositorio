package com.everis.apirest.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.TipoProducto;
import com.everis.apirest.model.repository.TipoProductoRepository;

@Service
public class TipoProductoServiceImpl implements TipoProductoService{
	
	@Autowired
	TipoProductoRepository tipoProductoRepository;
	
	@Override
    public TipoProducto obtenerTipoProductoPorCodigo(String codigo) throws Exception {
        return tipoProductoRepository.findByCodigo(codigo).orElseThrow(()-> new Exception("Tipo de Producto no encontrado."));
    }

}
