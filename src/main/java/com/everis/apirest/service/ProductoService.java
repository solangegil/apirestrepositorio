package com.everis.apirest.service;

import com.everis.apirest.model.entity.Producto;

public interface ProductoService {

	public Iterable<Producto> obtenerProductos();

	public Producto obtenerProductoPorId(Long id) throws Exception;

	public Producto guardarProducto(Producto producto) throws Exception;



}
