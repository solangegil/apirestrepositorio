package com.everis.apirest.controller.resource;

import java.math.BigDecimal;


import lombok.Data;


@Data
public class ProductoIGVResource {
	
	private Long id;
	private String nombre;
	private String codigo;
	private BigDecimal precio;
	private BigDecimal precioNeto;
	
}
