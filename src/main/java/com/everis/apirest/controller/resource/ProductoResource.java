package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
public class ProductoResource {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column
	private String nombre;
	@Column(unique = true)
	private String codigo;
	@Column
	private String descripcion;
	@Column
	private BigDecimal precio;
	@Column
	private TipoProductoResource tipoProducto;
	private Boolean activo;

}
