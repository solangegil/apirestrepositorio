package com.everis.apirest.controller;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.ProductoIGVResource;
import com.everis.apirest.controller.resource.ProductoReducidoResource;
import com.everis.apirest.controller.resource.ProductoResource;
import com.everis.apirest.controller.resource.TipoProductoResource;
import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.service.ProductoService;
import com.everis.apirest.service.TipoProductoService;


@RestController
public class ApiController {
	
	@Autowired
	ProductoService productoService;
	
	@Autowired
    TipoProductoService tipoProductoService;
	
	@Value("${igv}")
    BigDecimal igv;
	
	
	
	@PostMapping("/producto")
	public ProductoResource guardarProducto(@RequestBody ProductoReducidoResource request) throws Exception {
		Producto producto = new Producto();
		producto.setNombre(request.getNombre());
		producto.setActivo(true);
		producto.setCodigo((request.getCodigo()));
		producto.setDescripcion(request.getDescripcion() );
		producto.setPrecio(request.getPrecio());
		//producto.setTipoProducto(request.getCodigoTipoProducto());
		producto.setTipoProducto(tipoProductoService.obtenerTipoProductoPorCodigo(request.getCodigoTipoProducto()));
		
		Producto producto1 = new Producto();
		producto1 = productoService.guardarProducto(producto); //---------
		
		ProductoResource productoResource = new ProductoResource();
		productoResource.setId(producto1.getId());
		productoResource.setActivo(producto1.getActivo() );
		productoResource.setCodigo(producto1.getCodigo() );
		productoResource.setDescripcion(producto1.getDescripcion() );
		productoResource.setPrecio(producto1.getPrecio() );
		productoResource.setNombre(producto1.getNombre());
		
		TipoProductoResource tipoProductoResource=new TipoProductoResource();
        tipoProductoResource.setCodigo(producto.getTipoProducto().getCodigo());
        tipoProductoResource.setNombre(producto.getTipoProducto().getNombre());
		
        productoResource.setTipoProducto(tipoProductoResource);
		
		return productoResource;
	}
	
	
    @GetMapping("/productos")
    public  List<ProductoResource> obtenerProductos() throws Exception {
        List<ProductoResource> listado = new ArrayList<>();
        productoService.obtenerProductos().forEach(producto ->{
            ProductoResource productoResource =  new ProductoResource();
            productoResource.setId(producto.getId());
            productoResource.setNombre(producto.getNombre());
            productoResource.setCodigo(producto.getCodigo());           
            productoResource.setDescripcion(producto.getDescripcion());
            productoResource.setPrecio(producto.getPrecio());   
                TipoProductoResource tipoProductoResource=new TipoProductoResource();
                tipoProductoResource.setCodigo(producto.getTipoProducto().getCodigo());
                tipoProductoResource.setNombre(producto.getTipoProducto().getNombre());
            productoResource.setTipoProducto(tipoProductoResource);
            productoResource.setActivo(producto.getActivo());
            listado.add(productoResource);
        });
        return listado;
    }
 
	 
	 


    @GetMapping("/productos/{id}")
    public ProductoIGVResource obtenerUnProducto(@PathVariable Long id) throws Exception {
        Producto producto = productoService.obtenerProductoPorId(id);
        ProductoIGVResource productoIGVResource =  new ProductoIGVResource();
        productoIGVResource.setId(producto.getId());
        productoIGVResource.setNombre(producto.getNombre());
        productoIGVResource.setCodigo(producto.getCodigo());
        productoIGVResource.setPrecio(producto.getPrecio());
        productoIGVResource.setPrecioNeto(producto.getPrecio().add(producto.getPrecio().multiply(igv)));
        return productoIGVResource;
    }
	    
    @GetMapping("/igv")
    public BigDecimal obtenerIgv() {
        return igv;
    }

}
